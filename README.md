# Comicly
##1.Ime projekta (pokušajte smisliti originalno ime, a ne Aplikacija za pit' vodu)?

Comicly

##2.Koji problem rješava Vaša aplikacija (rješavanje nekog problema će vam dati ideju za izradu aplikacije i dodavanje funkcionalnosti te će povećati korisnost aplikacije)?
Aplikacija pomaže u praćenju pročitanih stripova ili onih koje korisnik žele pročitati.

##3.Koje su glavne funkcionalnosti Vaše aplikacije (navedite najmanje dvije glavne funkcionalnosti)?

Korisnik se može logirat ili registrirat čime kreira profil. Na profilu se vide pročitani stripovi ili oni koje želi pročitati. Na glavnom prozoru postoji pretraživanje stripova za koje podatke povlači iz baze. Stripovi koje koji se korisniku sviđaju može lajkati ili ih staviti u favorite koji će se također prikazati na profilnoj stranici. Stripove je moguće slikati kamerom te prikazati na profilnoj stranici ulogiranog korisnika. 

##4.Što od mogućnosti Android i web sustava kanite rabiti (CSS, HTML, više activitiya, RecyclerView, fragmente, sadržaj s weba...)?

Više activityja, Firebase baza, kamera, fragmenti.

##5.Koja su tri slična rješenja dostupna online ili na trgovinama aplikacijama u ovom trenutku?

Goodreads - https://play.google.com/store/apps/details?id=com.goodreads

League of Comic Geeks - https://play.google.com/store/apps/details?id=com.LeagueofComicGeeks.app2

Comixology - https://play.google.com/store/apps/details?id=com.iconology.comics
